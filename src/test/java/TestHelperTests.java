import control.TestHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.regex.Pattern;

public class TestHelperTests{
    @Test
    public void yearGeneration_commonCall_resultBetween1970And2022(){
        int i = TestHelper.yearGeneration();
        Assertions.assertTrue(i >= 1970 && i <= 2022);
    }
    @Test
    public void readFile_commonResult_resultTwoString(){
        HashMap<String,String> testData = TestHelper.readFile("C:\\Users\\vladi\\IdeaProjects\\hw_QA\\homework_for_qa\\src\\test\\resources\\test.txt");
        Assertions.assertTrue(testData.containsKey("23"));
        Assertions.assertTrue(testData.containsKey("11"));
        Assertions.assertEquals(2, testData.size());
        Assertions.assertEquals("Masha",testData.get("23"));
        Assertions.assertEquals("Glasha",testData.get("11"));
    }
    @Test
    public void longGeneration_commonResult_resultOneLong(){
        Assertions.assertTrue(TestHelper.longGeneration() instanceof Long);
    }
    @Test
    public void stringGeneration_commonResult_result(){
        String newLine = TestHelper.stringGeneration();
        String regexpr = "^[A-Z][a-z]+\\s[A-Z][a-z]+\\s[A-Z][a-z]+$";
        Assertions.assertTrue(Pattern.matches(regexpr,newLine));
    }
    @Test
    public void dataConverter_commonResult_oneData(){
        Assertions.assertEquals(TestHelper.dateConverter("21 Aug. 2021"), "21.08.2021");
        Assertions.assertEquals(TestHelper.dateConverter("21 May 2021"), "21.05.2021");
    }
    @Test
    public void doubleConverter_commonResult_dateAndInfinity(){
        Assertions.assertEquals(TestHelper.doubleConverter("90"), 90);
        Assertions.assertEquals(TestHelper.doubleConverter("pp"), Double.POSITIVE_INFINITY);
    }
}
