import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Calculator calculator = new Calculator();


        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine().replaceAll("\\s+", "");

        try {
            System.out.println(calculator.calculate(line));
        }
        catch (Exception exception){
            System.out.println(exception.getMessage());
        }

    }

}
