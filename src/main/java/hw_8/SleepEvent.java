package hw_8;

import java.util.Date;

public class SleepEvent extends Event{

    public SleepEvent(Date date) {
        super(date);
    }

    public String show() {
        return date.toString() + " Everybody sleeps. Nothing is happening";
    }
}
