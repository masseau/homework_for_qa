package hw_8;

import java.util.ArrayList;
import java.util.Date;

public class CharacterGenerator {
    final int maxArraySize = 10;
    ArrayList<String> firstName = new ArrayList<>(maxArraySize);
    ArrayList<String> secondName = new ArrayList<>(maxArraySize);


    public CharacterGenerator() {
        firstName.add("Albert");
        firstName.add("Adam");
        firstName.add("Barrett");
        firstName.add("Cliff");
        firstName.add("Elvin");
        firstName.add("Amanda");
        firstName.add("Catherine");
        firstName.add("Daisy");
        firstName.add("Ophelia");
        firstName.add("Theresa");

        secondName.add("Gordon");
        secondName.add("Murphy");
        secondName.add("Cook");
        secondName.add("London");
        secondName.add("Mason");
        secondName.add("Hardman");
        secondName.add("Bishop");
        secondName.add("Brian");
        secondName.add("Williams");
        secondName.add("Kingsman");
    }

    public Character bornCharacter(){
        int rndType = rnd(4);
        var firstNameIndex = rnd(maxArraySize);
        var secondNameIndex = rnd(maxArraySize);
        int characterId = Integer.parseInt(Integer.toString(secondNameIndex) + Integer.toString(firstNameIndex));

        if (rndType == 1){
            Peasant peasant = new Peasant(firstName.get(firstNameIndex), secondName.get(secondNameIndex), characterId);
            return peasant;
        }
        else if (rndType == 2){
            Witch witch = new Witch(firstName.get(firstNameIndex), secondName.get(secondNameIndex), characterId);
            return witch;
        }
        else if (rndType == 3){
            Vampire vampire = new Vampire(firstName.get(firstNameIndex), secondName.get(secondNameIndex), characterId);
            return vampire;
        }
        else {
            Warewolf warewolf = new Warewolf(firstName.get(firstNameIndex), secondName.get(secondNameIndex), characterId);
            return warewolf;
        }
    }

    public static int rnd(int max){
        return (int) (Math.random() * max);
    }


}
