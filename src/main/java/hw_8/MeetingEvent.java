package hw_8;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static hw_8.CharacterGenerator.rnd;
import static hw_8.Village.getMoonLightDays;

public class MeetingEvent extends Event{
    private Character character1;
    private Character character2;
    private Village village;
    private String message;


    public MeetingEvent(Date date, Character character1, Village village) {
        super(date);
        this.character1 = character1;
        this.village = village;
        character2 = village.getCharacter();
        while (character1.characterId == character2.characterId){
            character2 = village.getCharacter();
        }

        Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        var rndvmpr = rnd(100);


        if(((character1 instanceof Witch && !(character2 instanceof Witch))
                ||(character2 instanceof Witch && !(character1 instanceof Witch)))
                && (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY
        ) &&(calendar.get(Calendar.DATE) == 13 || calendar.get(Calendar.DATE) == 31)
        ){
            if(character1 instanceof Witch) {
                String character2firstname = character2.firstName;
                String character2secondname = character2.secondName;
                String character2classname = character2.getClass().getName();
                village.murderExeprion(character2.characterId);
                message = date.toString() + " MURDEREXCEPTION " + character2firstname + " " + character2secondname + " (" + character2classname
                        + ") killed by " + character1.firstName + " " + character1.secondName + " (" + character1.getClass().getName() +  ")";

            }
            else {
                String character1firstname = character1.firstName;
                String character1secondname = character1.secondName;
                String character1classname = character1.getClass().getName();
                village.murderExeprion(character1.characterId);
                message = date.toString() +" MURDEREXCEPTION " + character1firstname + " " + character1secondname + " (" + character1classname + ") killed by " + character2.firstName + " " + character2.secondName + " (" + character2.getClass().getName()+ ")";

            }
        }
        else if ((character1 instanceof Vampire && !(character2 instanceof Vampire))
                ||(character2 instanceof Vampire && !(character1 instanceof Vampire))){
            if(((character1 instanceof Vampire && !(character2 instanceof Vampire) && (character2 instanceof Peasant || character2 instanceof Witch))
                    && (rndvmpr > 0 && rndvmpr < 6))){
                village.makeVampire(character2);
                message = date.toString() + " " + character1.firstName + " " + character1.secondName + " (" + character1.getClass().getName() + ") turned into a vampire " + character2.firstName + " " + character2.secondName + " (" + character2.getClass().getName() + ")";
            }
            else if ((character2 instanceof Vampire && !(character1 instanceof Vampire) && (character1 instanceof Peasant || character1 instanceof Witch))
                    && (rndvmpr > 0 && rndvmpr < 6)){
                village.makeVampire(character1);
                message = date.toString() + " " + character2.firstName + " " + character2.secondName + " (" + character2.getClass().getName() + ") turned into a vampire " + character1.firstName + " " + character1.secondName + " (" + character1.getClass().getName() + ")";
            }
            else if((character1 instanceof Vampire && (rndvmpr > 5 && rndvmpr < 11))){
                String character2firstname = character2.firstName;
                String character2secondname = character2.secondName;
                String character2classname = character2.getClass().getName();
                village.murderExeprion(character2.characterId);
                message = date.toString() + " MURDEREXCEPTION " + character2firstname + " " + character2secondname + " (" + character2classname
                        + ") killed by " + character1.firstName + " " + character1.secondName + " (" + character1.getClass().getName() +  ")";
            }
            else if((character2 instanceof Vampire && (rndvmpr > 5 && rndvmpr < 11))){
                String character1firstname = character1.firstName;
                String character1secondname = character1.secondName;
                String character1classname = character1.getClass().getName();
                village.murderExeprion(character1.characterId);
                message = date.toString() +" MURDEREXCEPTION " + character1firstname + " " + character1secondname + " (" + character1classname + ") killed by " + character2.firstName + " " + character2.secondName + " (" + character2.getClass().getName()+ ")";
            }
            else if(character2 instanceof Vampire && (rndvmpr > 98 && rndvmpr < 100)){
                village.murderExeprion(character2.characterId);
                message = date.toString() + " " + character2.firstName + " " + character2.secondName + " (" + character2.getClass().getName() + ") left the Village.";
            }
            else if(character1 instanceof Vampire && (rndvmpr > 98 && rndvmpr < 100)){
                village.murderExeprion(character1.characterId);
                message = date.toString() + " " + character1.firstName + " " + character1.secondName + " (" + character1.getClass().getName() + ") left the Village.";
            }
            else {
                message = date.toString() + " " + character1.firstName + " " + character1.secondName + " (" + character1.getClass().getName() + ") drinks tea with " + character2.firstName + " " + character2.secondName + " (" + character2.getClass().getName() + ")";
            }
        }
        else if (((character1 instanceof Warewolf && !(character2 instanceof Warewolf))
                ||(character2 instanceof Warewolf && !(character1 instanceof Warewolf))) && getMoonLightDays(date)){
            if(character1 instanceof Warewolf) {
                String character2firstname = character2.firstName;
                String character2secondname = character2.secondName;
                String character2classname = character2.getClass().getName();
                village.murderExeprion(character2.characterId);
                message = date.toString() + " MURDEREXCEPTION " + character2firstname + " " + character2secondname + " (" + character2classname
                        + ") killed by " + character1.firstName + " " + character1.secondName + " (" + character1.getClass().getName() +  ")";

            }
            else {
                String character1firstname = character1.firstName;
                String character1secondname = character1.secondName;
                String character1classname = character1.getClass().getName();
                village.murderExeprion(character1.characterId);
                message = date.toString() +" MURDEREXCEPTION " + character1firstname + " " + character1secondname + " (" + character1classname + ") killed by " + character2.firstName + " " + character2.secondName + " (" + character2.getClass().getName()+ ")";

            }
        }
        else{
            message = date.toString() + " " + character1.firstName + " " + character1.secondName + " (" + character1.getClass().getName() + ") drinks tea with " + character2.firstName + " " + character2.secondName + " (" + character2.getClass().getName() + ")";
        }
    }

    @Override
    public String show() {
        return message;

    }
}
