package hw_8;

import java.util.Date;

public class LeaveEvent extends Event{
    private Character character;

    public LeaveEvent(Date date, Character character) {
        super(date);
        this.character = character;
    }

    @Override
    public String show() {
        return date.toString() + " " + character.firstName + " " + character.secondName + " (" + character.getClass().getName() + ") left the Village.";
    }
}
