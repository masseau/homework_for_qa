package hw_8;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static hw_8.CharacterGenerator.rnd;


public class Village {
   ArrayList<Character> characters = new ArrayList<Character>(50);
   ArrayList<Event> events = new ArrayList<Event>();

   public Village() {
      while (characters.size() != 25) {
         addCharacter();
      }
   }

   private void addCharacter(){
      var generator = new CharacterGenerator();
      var character = generator.bornCharacter();
      if (!characters.stream().anyMatch(c -> c.characterId == character.characterId)) {
         characters.add(character);
      }
   }

   private Character createCharacter() {
      var generator = new CharacterGenerator();
      var character = generator.bornCharacter();
      while (true) {
         int characterID = character.characterId;
         if (characters.stream().anyMatch(c -> c.characterId == characterID)) {
            character = generator.bornCharacter();
         }
         else{
            break;
         }

      }
      characters.add(character);
      return character;
   }

   public ArrayList<Event> GetHistory() {
      Calendar eventDate = new GregorianCalendar(1650, 0 , 1);
      Calendar endDate = new GregorianCalendar(1750, 1 , 27);

         while (!eventDate.after(endDate)) {
            //System.out.println(eventDate.toString());
            eventDate.add(Calendar.DAY_OF_WEEK, 1);
            int rndType = rnd(3);


            if (characters.size() != 50){
               if (rndType == 1){
                  events.add(new ArriveEvent(eventDate.getTime(), createCharacter()));
               }
               else if ( rndType == 2) {
                  events.add(new MeetingEvent(eventDate.getTime(), getCharacter(), this));
               }
               else if (characters.size() != 0) {
                  events.add(new LeaveEvent(eventDate.getTime(), leaveCharacter()));
               }

            }
            else {
               events.add(new SleepEvent(eventDate.getTime()));
            }

         }
         return events;
   }

   public Character leaveCharacter() {
      int charIndex = rnd(characters.size());
      var character = characters.get(charIndex);
      characters.remove(charIndex);
      return character;
   }

   public Character murderExeprion(int characterId) {
      var character = characters.stream().filter(c -> c.characterId != characterId).findFirst().get();
         characters.remove(character);
         return character;
   }

   public void makeVampire(Character character) {
      var charId = characters.indexOf(character);
      characters.set(charId, new Vampire(character.firstName, character.secondName, character.characterId));
      //System.out.println(characters.get(charId));
   }

   public Character getCharacter() {
      int charIndex = rnd(characters.size());
      var character = characters.get(charIndex);
      return character;
   }
   public static boolean getMoonLightDays(Date eventDate) {

      GregorianCalendar firstMoonLightDay = new GregorianCalendar(1650, 0, 1);
      Calendar date = new GregorianCalendar();
      date.setTime(eventDate);
      while (firstMoonLightDay.before(date) || firstMoonLightDay.equals(date)) {
         if (firstMoonLightDay.equals(date)) {
            return true;
         }
         firstMoonLightDay.add(Calendar.DATE, 28);
      }
      return false;
   }

}
