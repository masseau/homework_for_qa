package hw_8;

import java.util.Calendar;
import java.util.Date;

public class ArriveEvent extends Event{
    Character character;

    public ArriveEvent(Date date, Character character) {
        super(date);
        this.character = character;
    }

    @Override
    public String show() {

        return date.toString() + " " + character.firstName + " " + character.secondName + " (" + character.getClass().getName() + ") appeared in the Village.";
    }
}
