package control;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class TestHelper {
    private static final Random rnd = new Random();
    private static final int minRationalSize = 5;
    private static final int maxRationalSize = 10;

    public static int yearGeneration(){
        return 1970 + rnd.nextInt(53);
    }
    public static Long longGeneration(){
        return rnd.nextLong();
    }
    public static String stringGeneration(){
        return wordGeneration().append(" ").append(wordGeneration().append(" ").append(wordGeneration())).toString();
    }
    public static HashMap<String,String> readFile(String fileName){
        HashMap<String,String> data = new HashMap<>();
        try {
            InputStreamReader reader = new InputStreamReader(new FileInputStream(fileName));
            Scanner scanner = new Scanner(reader);
            while (scanner.hasNext()){
                String newLine = scanner.nextLine();
                String[] pair = newLine.split(" :: ");
                data.put(pair[0],pair[1]);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return data;
    }
    public static String dateConverter(String oldDate){
        String[] tokens = oldDate.split(" ");
        return tokens[0] + monthNumber(tokens[1]) + tokens[2];
    }
    public static Double doubleConverter(String oldString){
        try {
            return Double.parseDouble(oldString);
        }catch (NumberFormatException e){
            return Double.POSITIVE_INFINITY;
        }
    }

    private static StringBuilder wordGeneration(){
        int lengthWord =  minRationalSize + rnd.nextInt(maxRationalSize - minRationalSize);
        StringBuilder result = new StringBuilder();
        result.append((char)('A' + rnd.nextInt(26)));
        for(int i = 0; i < lengthWord - 1; i++ ){
            result.append((char)('a' + rnd.nextInt(26)));
        }
        return result;
    }
    private static String monthNumber(String month){
        return switch (month) {
            case "Jan." -> ".01.";
            case "Feb." -> ".02.";
            case "Mar." -> ".03.";
            case "Apr." -> ".04.";
            case "May" -> ".05.";
            case "Jun." -> ".06.";
            case "Jul." -> ".07.";
            case "Aug." -> ".08.";
            case "Sep." -> ".09.";
            case "Oct." -> ".10.";
            case "Nov." -> ".11.";
            case "Dec." -> ".12.";
            default -> "";
        };
    }
}




//      -  для каждого метода написаны тесты, проверяющие основной функционал – не менее 1 теста на метод

//       -  форматирование кода, соблюдение camelCase, отсутствие лишних пробелов, двойных пустых строк
