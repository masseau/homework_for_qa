import java.util.Objects;
import java.util.regex.Pattern;


public class Calculator {

    private String makeOneOperation(String expr, String sign) throws Exception {

//        expr = replaceDoubleMinus(expr);
        int sign_lr = indexOf(expr, sign);
        int sign_ll = 0;
        for (int i = sign_lr - 1; i >= 0; i--) {
            if (expr.charAt(i) == '-' || expr.charAt(i) == '+' || expr.charAt(i) == '*' || expr.charAt(i) == '/'){
                sign_ll = i;
                break;
            }
        }

        if (sign_ll != 0){
            sign_ll = sign_ll + 1;
        }
        else {
            sign_ll = 0;
        }

        int sign_rr = indexOf(expr, sign);
        int sign_rl = expr.length();
        for (int i = sign_rr + 1; i < expr.length(); i++) {
            if (i == sign_rr + 1 && expr.charAt(i) == '-'){
                continue;
            }
            if (expr.substring(i, i+1).equals("-") || expr.substring(i, i+1).equals("+") || expr.substring(i, i+1).equals("*") ||expr.substring(i, i+1).equals("/")){
                sign_rl = i;
                break;
            }
        }

        var n1String = expr.substring(sign_ll, sign_lr);
        var n2String = expr.substring(sign_rr+1, sign_rl);
        if (n1String.isEmpty() || n2String.isEmpty()) throw new Exception("Некорректное выражение");
        Double n1 = Double.parseDouble(n1String);
        Double n2 = Double.parseDouble(n2String);

        String result = expr.substring(0, sign_ll) + Double.toString(makeOperation(n1, n2, sign))  + expr.substring(sign_rl);

        return result;
    }

    private int indexOf(String expr, String sign) {
        if (sign == "-" && expr.charAt(0) == '-') {
            return expr.indexOf(sign, 1);
        }
        else {
            return expr.indexOf(sign);
        }
    }

    private String makeAllOperation(String expr, String sign) throws Exception {
            while (indexOf(expr, sign) != -1) expr = makeOneOperation(expr, sign);
            return expr;

    }

    private double makeOperation(double n1, double n2, String sign) throws Exception {
        Double result = null;
        if (Objects.equals(sign, "*")) {
            result = n1 * n2;
        } else if (Objects.equals(sign, "/") && (n2 != 0)) {
            result = n1 / n2;
        } else if (Objects.equals(sign, "+")) {
            result = n1 + n2;
        } else if (Objects.equals(sign, "-")) {
            result = n1 - n2;
        } else if (Objects.equals(sign, "/") && (n2 == 0)) {
            throw new Exception("Деление на ноль. Так нельзя-__-");
        }

        return result;

    }

    private String calculateWithoutBracket(String expr) throws Exception {

        expr = makeAllOperation(expr, "/");
        expr = makeAllOperation(expr, "*");
        expr = makeAllOperation(expr, "+");
        expr = makeAllOperation(expr, "-");
        return expr;
    }


    private String foundExpressionInBracket(String expr) throws Exception {
        int value_right = expr.indexOf(')');
        int value_left = 0;
        for (int i = value_right; i > 0; i--) {
            if (expr.substring(i, i+1).equals("(")){
                value_left = i;
                break;
            }
        }

        return expr.substring(0, value_left) + calculateWithoutBracket(expr.substring(value_left+1, value_right))+ expr.substring(value_right + 1);
    }

    private String calculateAllBracket(String expr) throws Exception {
        while (indexOf(expr, ")") != -1) {
            expr = foundExpressionInBracket(expr);
        }
        return expr;
    }

    public String calculate(String expr) throws Exception {
        String regex = "^[\\d\\(\\)\\*/\\+\\-\\.]+$";
        //Pattern pattern = Pattern.compile(regex);

        if(!Pattern.matches(regex,expr)){
            throw new Exception("Есть недопустимые символы");
        }
        else {
            return calculateWithoutBracket(calculateAllBracket(expr));
        }
    }
}
