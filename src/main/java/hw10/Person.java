package hw10;

public class Person {
    String name;
    int sum;
    int age;

    public Person(String name, int sum, int age) {
        this.name = name;
        this.sum = sum;
        this.age = age;
    }

    public String toJSON(){
        return "{\"name\":" + "\"" + name + "\"," +
                "\"" + "sum\":" +  sum + "," +
                    "\"" +"age\":" +  age + "}" ;
    }

    public static Person fromJSON(String jsonPerson){
        int namePos = jsonPerson.indexOf("\"name\":");
        int sumPos = jsonPerson.indexOf("\"sum\":");
        int agePos = jsonPerson.indexOf("\"age\":");

        String name1 = jsonPerson.substring(namePos + 7, sumPos).trim();
        String sum1 = jsonPerson.substring(sumPos + 6, agePos).trim();
        String age1 = jsonPerson.substring(agePos + 6).trim();

        String name2 = name1.substring(1, name1.length()-2);
        int sum2 = Integer.parseInt(sum1.substring(0, sum1.length()-1));
        int age2 = Integer.parseInt(age1.substring(0, age1.length()-1));

        return new Person(name2, sum2, age2);
    }
}


